﻿using UnityEngine;
using System.Collections;

public class SoundEffectTrigger : MonoBehaviour {

    public AudioClip clip;
    public AudioSource source;
    private bool hasPlayed;

	// Use this for initialization
	void Start () {
        hasPlayed = false;
        if (clip != null) {
            source.clip = clip;
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (source.clip != null)
        {
            if (!hasPlayed)
            {
                hasPlayed = true;
                source.Play();
            }
        }
    }
}
