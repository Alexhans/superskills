﻿using UnityEngine;
using System.Collections;

public class FaceObject : MonoBehaviour {

	public Transform target;
	// Use this for initialization
	void Start () {
		
	}

	void Update () {
		Vector3 relativePos = (target.position + new Vector3 (0, 0, 0)) - transform.position;
        relativePos.y = 0;
		Quaternion rotation = Quaternion.LookRotation (relativePos);

		//Quaternion current = transform.localRotation;

		gameObject.transform.localRotation = rotation;//Quaternion.Slerp (current, rotation, Time.deltaTime);
		//transform.Translate(0, 0, 3 * Time.deltaTime);

	}
}
