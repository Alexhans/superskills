﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameStaticClass : MonoBehaviour {

    public static int level = Application.loadedLevel;
    public static bool win = false;
    public static bool death = false;

	public static bool cant_jump = false;
	public static bool partially_blind = false;
	public static bool fully_blind = false;
	public static bool confused = false;

    public static void LoadNextLevel()
    {
        level += 1;
        Application.LoadLevel(level);
        ResetLevel();
    }

    public static void ReloadLevel()
    {
        Application.LoadLevel(level);
        ResetLevel();
    }

    public static void ResetLevel()
    {
        win = false;
        death = false;
    }

	public static void CureEverything() {
		cant_jump = false;
		partially_blind = false;
		fully_blind = false;
		confused = false;
		Debug.Log ("cure everything");
	}

	public static void toggleFullyBlind() {
		fully_blind = !fully_blind;
		partially_blind = false;
		Debug.Log ("toggle fully blind");
	}

	public static void togglePartialBlind() {
		partially_blind = !partially_blind;
		fully_blind = false;
		Debug.Log ("toggle partial blind");
	}

	public static void toggleCantJump() {
		cant_jump = !cant_jump;
		GameObject[] playerList = GameObject.FindGameObjectsWithTag("Player");
		foreach (GameObject player in playerList) {
			PlayerController controller = player.GetComponent<PlayerController> ();
			if(controller != null) {
				controller.piernaLesionada = cant_jump;
			}
		}
		Debug.Log ("toggle cant jump");

	}

	public static void toggleConfused() {
		confused = !confused;
		GameObject[] playerList = GameObject.FindGameObjectsWithTag("Player");
		foreach (GameObject player in playerList) {
			PlayerController controller = player.GetComponent<PlayerController> ();
			if(controller != null) {
				controller.confundido = confused;
			}
		}
		Debug.Log ("toggle confused");
	}
}
