﻿using UnityEngine;
using System.Collections;

public class LoseOnCollide : MonoBehaviour {
    public AudioSource source;
    void Start()
    {
            source.Play();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("log SAME level change");
            gameObject.collider.isTrigger = false;

			if(source != null)
            	source.Play();
            //GameStaticClass.ReloadLevel();
            StartCoroutine(aboutToReload());
        }
    }

    IEnumerator aboutToReload()
    {
        yield return new WaitForSeconds(0.2f);
        GameStaticClass.ReloadLevel();
        source.Stop();
    }
}