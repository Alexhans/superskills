﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaitABitHalfBlind : MonoBehaviour {
    private ControlBlindness what;
    public float secondsToBeBlind;
    public Image img;
	// Use this for initialization
	void Start () {
        what = gameObject.GetComponent<ControlBlindness>();
        StartCoroutine(waitToBlind());
	}
	
	// Update is called once per frame
	void Update () {
	}

    IEnumerator waitToBlind()
    {
        yield return new WaitForSeconds(secondsToBeBlind);
        what.setHalfBlind(true);
        img.active = true;
        
    }
}
