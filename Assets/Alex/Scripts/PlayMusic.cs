﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayMusic : MonoBehaviour {
	private AudioSource aud;
	public AudioClip first;
	public AudioClip second;
	public AudioClip third;
	private int count;
	// Use this for initialization
	void Start () {
		aud = gameObject.GetComponent<AudioSource> ();
		count = Random.Range (0, 2);
		aud.Play ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!aud.isPlaying) {
			RandomlyChange();

			aud.Play();
		}
	}

	void RandomlyChange() {
		List<Object> test = new List<Object> ();
		count = Random.Range (0, 2);
		Debug.Log (count);
		if (count == 0) {
						test.Add (first);
						test.Add (second);
						test.Add (third);
				} else if (count == 1) {
			test.Add (third);
			test.Add (first);
			test.Add (second);
				} else {
			test.Add (second);
			test.Add (first);
			test.Add (third);
				}

		if(aud.clip.ToString() == test[0].ToString()) aud.clip = test[1] as AudioClip;
		else if(aud.clip.ToString() == test[1].ToString()) aud.clip = test[2] as AudioClip;
		else aud.clip = test[0] as AudioClip;

		Debug.Log (aud.clip.ToString ());
	}


}
