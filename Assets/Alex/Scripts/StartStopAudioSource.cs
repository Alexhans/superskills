﻿using UnityEngine;
using System.Collections;

public class StartStopAudioSource : MonoBehaviour {

    public AudioSource start;
    public AudioSource stop;

    void OnTriggerEnter(Collider col) {
        if (col.tag == "Player")
        {
            Debug.Log("parent object is Player so start playing");
            if (start != null)
            {
                start.Play();
            }
            AudioSource audioSource = gameObject.GetComponentInChildren<AudioSource>();
            Debug.Log(audioSource.tag);
            audioSource.Stop();
        }
    }
}

