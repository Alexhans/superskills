﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ControlBlindness : MonoBehaviour {
    public Canvas blindCanvas;
    public Canvas halfBlindCanvas;
    private GameObject blind; 
    private GameObject half; 
    private bool isBlind;
    private bool isHalf;
	// Use this for initialization
	void Start () {
        isBlind = false;
        isHalf = false;
        blind = null;
        half = null;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            setBlind(true);
            setHalfBlind(false);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            setHalfBlind(true);
            setBlind(false);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            destroyAll();
        }
	}

    public void destroyAll()
    {
        Destroy(blind);
        Destroy(half);
    }
    public void setBlind(bool set)
    {
        if (blindCanvas != null)
        {
            if (set && !isBlind) { 
                blind = Instantiate(blindCanvas.gameObject, new Vector3(), Quaternion.identity) as GameObject;
                isBlind = true;
            }
            else
            {
                if (blind != null) Destroy(blind);
                isBlind = false;
            }
        }
    }

    public void setHalfBlind(bool set)
    {
        if (halfBlindCanvas != null)
        {
            if (set && !isHalf) { 
                half = Instantiate(halfBlindCanvas.gameObject, new Vector3(), Quaternion.identity) as GameObject;
                isHalf = true;
            }
            else
            {
                if (half != null) Destroy(half);
                isHalf = false;
            }
        }
    }
}
