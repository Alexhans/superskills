﻿using UnityEngine;
using System.Collections;

public class RotateAsteroid : MonoBehaviour {

	public Transform target;
	public float speed;

	void FixedUpdate()  {
		Vector3 relativepos = target.position - transform.position;
		Quaternion rotation = Quaternion.LookRotation (relativepos);

		Quaternion current = transform.localRotation;

		transform.localRotation = Quaternion.Slerp (current, rotation, Time.deltaTime * speed);
		transform.Translate (0, 0, 30 * Time.deltaTime);

		transform.rigidbody.angularVelocity = Random.insideUnitSphere * 4.0f;
	}
}
