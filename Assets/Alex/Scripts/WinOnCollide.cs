﻿using UnityEngine;
using System.Collections;

public class WinOnCollide : MonoBehaviour {
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            gameObject.collider.isTrigger = false;
            GameStaticClass.LoadNextLevel();
        }
    }
}
