﻿using UnityEngine;
using System.Collections;

public class CrearPelotas : MonoBehaviour {

	public GameObject ball;
	public float tiempo;
	public float coordXMin;
	public float coordXMax;
	public float coordY;
	public float coordZ;
	public int cantBolasMax;
	public float esperaInicial;
	public float esperaEntreBolas;
	
	// Use this for initialization
	void Start () {
		StartCoroutine (CrearBolas ());
	}
	
	IEnumerator CrearBolas(){
		yield return new WaitForSeconds (esperaInicial);
		int i = 0;
		while (true) {

			while (i<cantBolasMax){
				Vector3 puntoCreacion = new Vector3 (Random.Range (coordXMin,coordXMax),coordY,coordZ);

				GameObject clone = Instantiate(ball, puntoCreacion, Quaternion.identity) as GameObject;
				//clone.collider.mat = Random.Range(0.0f, 0.8f);
				i++;
				yield return new WaitForSeconds (esperaEntreBolas);
			}

			i=0;
			yield return new WaitForSeconds (esperaInicial);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
