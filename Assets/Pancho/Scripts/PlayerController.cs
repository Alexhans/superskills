﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerController : MonoBehaviour {

	public Dictionary<string, Vector3> teclasMovimiento;
	public float speed;
	public float velocidadLimite;
	public float fuerzaSalto;
	public int tiempoEntreSaltos;
	public int contMaxEntreSaltos;
	public int timerCambioTeclas;
	public bool confundido;
	public bool piernaLesionada;

	public AudioSource audioSalto;
	private RaycastHit rayCastInfo;
	private bool teclaPresionada;
	private int segundoComienzoSalto;
	private int minutoComienzoSalto;
	private int contadorTiempoSalto;
	private int contadorTiempoTeclas;

    private List<KeyCode> arrows;
    private List<KeyCode> awsd;

	//public Text texto;
	
	void Start() {
		rayCastInfo = new RaycastHit ();
		teclaPresionada = false;
		Screen.showCursor = false;
		contadorTiempoSalto = 0;
        resetKeys();
		contadorTiempoTeclas = 0;
	}

    private void resetKeys()
    {
        arrows = new List<KeyCode>();
        awsd = new List<KeyCode>();

        arrows.Add(KeyCode.UpArrow);
        arrows.Add(KeyCode.DownArrow);
        arrows.Add(KeyCode.RightArrow);
        arrows.Add(KeyCode.LeftArrow);

        awsd.Add(KeyCode.W);
        awsd.Add(KeyCode.S);
        awsd.Add(KeyCode.D);
        awsd.Add(KeyCode.A);
    }

    void Shuffle() {
        for (int i = 0; i < awsd.Count; i++) {
            KeyCode temp = awsd[i];
            int randomIndex = UnityEngine.Random.Range(i, awsd.Count);
            awsd[i] = awsd[randomIndex];
            awsd[randomIndex] = temp;
        }

        for (int i = 0; i < arrows.Count; i++) {
            KeyCode temp = arrows[i];
            int randomIndex = UnityEngine.Random.Range(i, arrows.Count);
            arrows[i] = awsd[randomIndex];
            arrows[randomIndex] = temp;
        }

        Debug.Log(arrows);
    }


	void Update() {
		debug_estados();
	}
	void FixedUpdate() {

		if ((confundido) && (contadorTiempoTeclas == 0)) {
			contadorTiempoTeclas = timerCambioTeclas;
			Shuffle();
		}
		if (contadorTiempoTeclas > 0) {
			contadorTiempoTeclas--;
		}

		
		if (Input.GetKey ("space") && pasoElTiempoDeSalto (tiempoEntreSaltos) && !piernaLesionada) {
			rigidbody.AddForce (new Vector3 (0.0f, fuerzaSalto, 0.0f) * speed * Time.deltaTime, ForceMode.VelocityChange);
			minutoComienzoSalto = DateTime.Now.Minute;
			segundoComienzoSalto = DateTime.Now.Second;
			contadorTiempoSalto = contMaxEntreSaltos;
			audioSalto.Play();
		}

		if (puedeMoverse()) {
						if (confundido) {
								if (Input.GetKey (arrows[0]) || Input.GetKey (awsd[0]))
										rigidbody.AddForce (transform.forward * speed * Time.deltaTime);
								if (Input.GetKey (arrows[1]) || Input.GetKey (awsd[1]))
										rigidbody.AddForce (-transform.forward * speed * Time.deltaTime);
								if (Input.GetKey (arrows[2]) || Input.GetKey (awsd[2]))
										rigidbody.AddForce (transform.right * speed * Time.deltaTime);
								if (Input.GetKey (arrows[3]) || Input.GetKey (awsd[3]))
										rigidbody.AddForce (-transform.right * speed * Time.deltaTime);
						} else {
								if (Input.GetKey (arrows[0]) || Input.GetKey (awsd[0]))
										rigidbody.AddForce (transform.forward * speed * Time.deltaTime);
								if (Input.GetKey (arrows[1]) || Input.GetKey (awsd[1]))
										rigidbody.AddForce (-transform.forward * speed * Time.deltaTime);
								if (Input.GetKey (arrows[2]) || Input.GetKey (awsd[2]))
										rigidbody.AddForce (transform.right * speed * Time.deltaTime);
								if (Input.GetKey (arrows[3]) || Input.GetKey (awsd[3]))
										rigidbody.AddForce (-transform.right * speed * Time.deltaTime);
						}	
		}

		if( (rigidbody.velocity.magnitude > velocidadLimite) && (rigidbody.velocity.y <= -0.1) )
		{
			rigidbody.velocity = rigidbody.velocity.normalized * velocidadLimite;
		}
		
		
		if (Input.GetKeyDown ("e") && (!teclaPresionada) ) {
			teclaPresionada = true;
			interactuar();

		}
		if(Input.GetKeyUp ("e") )
			teclaPresionada = false;

	}

	//bool estaEnElPiso() {
	//	return Physics.Raycast(transform.position, Vector3.up, transform.localScale.y);
	//}

	void debug_estados() {
		if (Input.GetKeyDown (KeyCode.Alpha1)) {
				GameStaticClass.toggleCantJump ();
		} else if (Input.GetKeyDown (KeyCode.Alpha2)) {
				GameStaticClass.toggleConfused ();
		} else if (Input.GetKeyDown (KeyCode.Alpha3)) {
				GameStaticClass.togglePartialBlind();
		} else if (Input.GetKeyDown (KeyCode.Alpha4)) {
				GameStaticClass.toggleFullyBlind ();

		} else if (Input.GetKeyDown (KeyCode.Alpha0)) {
			GameStaticClass.CureEverything();
		}

	}

	bool pasoElTiempoDeSalto(int tiempo){
		DateTime tiempoActual = DateTime.Now;
//		print ("minutoCom: " + tiempoActual.Minute.ToString() + "segundosCom: " + tiempoActual.Second.ToString() + "\n");
//		print ("minuto: " + minutoComienzoSalto.ToString() + "segundos: " + segundoComienzoSalto.ToString());
		if( (tiempoActual.Minute > minutoComienzoSalto) && (tiempoActual.Second > tiempo) )
		{
			return true;
		}
		if (tiempoActual.Second - tiempo >= segundoComienzoSalto) {
			return true;
		}
		return false;
	}

	bool puedeMoverse()
	{
		if (contadorTiempoSalto > 0) {
			contadorTiempoSalto--;
			return false;
		}
		return true;

	}

	void interactuar(){
		Vector3 vectorA = new Vector3 (0.0f, 1.0f, 0.0f);
		Physics.Raycast (transform.position, transform.position - vectorA + transform.forward, out rayCastInfo, 1.0f);

		if (rayCastInfo.collider != null) {
			Debug.Log (rayCastInfo.collider.tag);
			Collider other = rayCastInfo.collider;
			if (other.tag == "Boton") {
				bool botonActivado = (other.renderer.material.color == Color.green);
				if (botonActivado) {
					other.renderer.material.color = Color.magenta;
				} else {
					other.renderer.material.color = Color.green;
				}
			}
			else if(other.tag == "Levantable")
			{
				/*other.collider.transform.DetachChildren();
				GameObject hitObject = other.collider.gameObject;
				hitObject.transform.parent = gameObject.transform;*/
				//other.collider.transform.DetachChildren();
				//other.collider.transform.GetChild.transform.parent = gameObject.transform; //  (GameObject) other.collider.GetComponentInChildren<GameObject>();
				//childHitObject.parent = gameObject.transform;
				//Transform tranCamara = gameObject.GetComponentsInChildren<Transform>()[0];
				//hitObject.transform.localEulerAngles.x = tranCamara.rotation.y;
				//other.renderer.material.color = Color.magenta;
				/*Rigidbody rgbody = other.GetComponentInParent<Rigidbody>();
				Transform trans = other.GetComponentInParent<Transform>();
				trans.position = transform.position + transform.forward;
				other.transform.position = transform.position + transform.forward;
				rgbody.useGravity = false;*/
				//other.transform.position = transform.position + transform.forward;
				//other.rigidbody.useGravity = false;
			}
		}
	}
}
